package edu.ufrn.lapps

object EnvConfig {
    val VIRTUAL_CPU_PATH = getCpuPath(virtual = true)
    val VIRTUAL_THERMAL_PATH = getThermalPath(virtual = true)
    val HARDWARE_CPU_PATH = getCpuPath(virtual = false)
    val HARDWARE_THERMAL_PATH = getThermalPath(virtual = false)
}

// TODO these functions will load the paths from a file so it can have more compatibility
fun getCpuPath(virtual: Boolean = false) : String {
    return "/sys/devices/system/cpu"
}

fun getThermalPath(virtual: Boolean = false) : String {
    return if (virtual) {
        "/sys/devices/virtual/thermal"
    } else {
        "/sys/class/thermal"
    }
}