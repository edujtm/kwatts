package edu.ufrn.lapps.utils

import edu.ufrn.lapps.data.Timeout
import java.io.File
import java.lang.RuntimeException

/**
 * Runs a process in the [workingDir] Directory from a command specified on the String argument.
 * The process will be run indefinetely if a [timeout] is not defined.
 *
 * This is necessary because most android application are interactive, so if you want to run it
 * with a deterministic amount of time, it's better to do this programmatically.
 */
fun String.runProcess(workingDir : File? = null, timeout: Timeout? = null) {
    val process = ProcessBuilder(this.split(" "))
        .directory(workingDir)
        .start()

    if (timeout != null && !process.waitFor(timeout.time, timeout.unit)) {
        process.destroy()
        throw RuntimeException("process execution for command [${this}] timed out in $timeout seconds")
    } else if (timeout == null) {
        process.waitFor()
    }

    if (process.exitValue() != 0) {
        throw RuntimeException("process for command [${this}] finished with non-zero return value.")
    }
}

/**
 * Will transform the Iterable values into an 8 bit hexadecimal mask
 * representation the values must be on the range [0 - 7].
 */
fun Iterable<Int>.toHexMask() : String {
    val mask = this.map { value -> 1 shl value }
        .fold(0) { acc, bitset -> acc or bitset }
    return "%02X".format(mask)
}