package edu.ufrn.lapps.utils

import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * This function acts similar to [zip] but it pairs elements until the
 * longest iterable has been exhausted. When the shortest iterable elements
 * are exhausted, [merge] will pair the remaining elements with null.
 *
 * Ex: list1 = [1, 2, 3, 4, 5]
 *     list2 = [6, 7, 8, 9, 10, 11, 12, 13]
 * the pairs will be:
 *     (1, 6) -> (2, 7) -> (3, 8) -> (4, 9) -> (5, 10) -> (null, 11) -> (null, 12) -> (null, 13)
 *
 * The zip function works only until the shortest iterable has been exhausted.
 * @see kotlin.collections.zip
 */
infix fun <T,R> Iterable<T>.merge(other: Iterable<R>) : Iterator<Pair<T?, R?>> {
    return iterator {
        val mine = this@merge.iterator()
        val theirs = other.iterator()
        while (mine.hasNext() || theirs.hasNext()) {
            var first : T? = null
            var second : R? = null
            if (mine.hasNext()) {
                first = mine.next()
            }
            if (theirs.hasNext()) {
                second = theirs.next()
            }
            yield(Pair(first, second))
        }
    }
}

/**
 * Measures the amount of time used to run the block of code in the [block] argument.
 * The elapsed time is measured in nanoseconds for precision while the starting and
 * finishing time are unix timestamps.
 *
 * @param block the block which the elapsed time is to be measured
 * @return a triple with elapsed time, starting time and finishing time respectively.
 */
fun timeit(block: () -> Unit) : Triple<Long, Long, Long> {
    val start = System.currentTimeMillis()
    val startTime = System.nanoTime()
    block()
    val elapsed = System.nanoTime() - startTime
    val end = System.currentTimeMillis()
    return Triple(elapsed, start, end)
}

/**
 * Run [block] until either it returns a value or the [time] in units of [unit] has passed.
 * If the [block] timeouts, the return will be null.
 */
fun <T> timeout(time : Long, unit: TimeUnit, block: suspend () -> T) = runBlocking {
    val total = unit.toMillis(time)
    return@runBlocking withTimeoutOrNull(total) {
        return@withTimeoutOrNull block()
    }
}

/**
 * Transforms a unix timestamp into a datetime with a specified [format] and a possible
 * [timezone] that is by default UTC.
 *
 * @receiver the unix timestamp in milliseconds
 * @param format The datetime format (e.g. dd/MM/yyyy HH:mm)
 * @param timezone
 */
fun Long.toDatetime(format: String, timezone: String = "Etc/UTC") : String? {
    val date = Date(this)
    val datefmt = SimpleDateFormat(format)
    datefmt.timeZone = TimeZone.getTimeZone(timezone)
    return datefmt.format(date)
}