package edu.ufrn.lapps.commands

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.validate
import com.github.ajalt.clikt.parameters.types.choice
import com.github.ajalt.clikt.parameters.types.long
import edu.ufrn.lapps.system.Processor
import edu.ufrn.lapps.system.processorHeader

class Temperature : CliktCommand(help="""
    Checks for temperature and frequency readings on the /sys filesystem.
    
    This command is meant to be used to check if the environment is correctly 
    configured or if the OS stopped execution due to high temperature when
    the benchmark was running.
""".trimIndent()) {
    private val sampling by option("-s", "--sampling", help="Time between reads from filesystem in milliseconds").long().default(1000L).validate {
        if (it <= 0) fail("sampling period must be greater than 0")
    }
    private val format by option("-f", "--format", help="output format").choice("csv", "human").default("human")

    override fun run() {
        when (format) {
            "csv" -> printCsvFormat()
            "human" -> printHumanFormat()
        }
    }

    private fun printCsvFormat() :Nothing {
        println(processorHeader(separator = ","))
        while (true) {
            val vprocessor = Processor.getState(virtual = true)
            val hprocessor = Processor.getState(virtual = false)
            println(vprocessor.format(separator = ","))
            println(hprocessor.format(separator = ","))
            Thread.sleep(sampling)
        }
    }

    private fun printHumanFormat() : Nothing {
        println(processorHeader(separator = "\t"))
        while (true) {
            val vprocessor = Processor.getState(virtual = true)
            val hprocessor = Processor.getState(virtual = false)
            println(vprocessor.format(separator = "\t"))
            println(hprocessor.format(separator = "\t"))
            Thread.sleep(sampling)
        }
    }
}
