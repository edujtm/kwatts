package edu.ufrn.lapps.commands.custom

import com.github.ajalt.clikt.parameters.groups.OptionGroup
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.clikt.parameters.options.split
import com.github.ajalt.clikt.parameters.options.validate
import com.github.ajalt.clikt.parameters.types.int

class BigCoreOptions : OptionGroup() {
    val bigCores by option("-b", "--big", help = "number of big cores").int()
        .required()
        .validate {
            if (it !in 1..2) fail("Index for big cluster is out of bounds. values must be in range [1-4]")
        }
    val frequencies by option("-f", help = "frequencies for big cluster in Hz, separated by commas")
        .split(",")
        .required()
}

class LittleCoreOptions : OptionGroup() {
    val littleCores by option("-L", "--LITTLE", help = "number of LITTLE cores").int().required()
        .validate {
            if (it !in 1..6) fail("Index for LITTLE cluster is out of bounds. Values must be in range [1-4]")
        }
    val frequencies by option("-q", help = "frequencies for LITTLE cluster in Hz, separated by commas")
        .split(",")
        .required()
}