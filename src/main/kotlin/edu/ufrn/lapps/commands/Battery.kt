package edu.ufrn.lapps.commands

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.validate
import com.github.ajalt.clikt.parameters.types.file
import com.github.ajalt.clikt.parameters.types.long
import edu.ufrn.lapps.data.Timeout
import edu.ufrn.lapps.system.BatterySys
import edu.ufrn.lapps.utils.timeout
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import java.io.File
import java.io.PrintWriter
import java.lang.IllegalArgumentException
import java.util.concurrent.TimeUnit

class Battery : CliktCommand(help = """
    Get battery measurements from /sys filesystem
""".trimIndent()) {

    val sampling by option("-s", "--sampling", help = "battery information sampling rate in milliseconds").long().default(500L)
    val outputPath by option("-o", "--output", help = "output file").file()
    val timeoutString by option("-t", "--timeout", help = "will keep running until execution time is greater than the timeout. Units in Milliseconds")
        .validate {
           Timeout.PATTERN.matchEntire(it) ?: fail("Please specify correct units for timeout. One of [ms,s,m,h,d]. " +
                                       "--help for more info")
        }

    override fun run() {
        if (timeoutString != null) {
            val (time, unit) = parseTimeout(timeoutString!!)
            timeout(time, unit) {
                mainloop()
            }
        } else {
            runBlocking { mainloop() }
        }
    }

    suspend fun mainloop() {
        val output = getOutput(outputPath)
        val header = "timestamp,${BatterySys.getCsvHeader()}"
        output.println(header)
        while (true)  {
            try {
                val timestamp = System.currentTimeMillis()
                val dataline = "$timestamp,${BatterySys.format(separator = ",")}"
                output.println(dataline)
                delay(sampling)
            } catch (e : TimeoutCancellationException) {
                break
            }
        }
    }

    private fun parseTimeout(timeStr: String) : Timeout {
        return try {
            Timeout.parse(timeStr)
        } catch (e: IllegalArgumentException) {
            Timeout(10, TimeUnit.SECONDS)
        }
    }

    private fun getOutput(outputFile : File?) : PrintWriter {
        val stream = outputFile?.outputStream() ?: System.out
        return PrintWriter(stream, true)
    }

}