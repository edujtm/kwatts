package edu.ufrn.lapps.commands

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.CliktError
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.multiple
import com.github.ajalt.clikt.parameters.arguments.validate
import com.github.ajalt.clikt.parameters.groups.cooccurring
import com.github.ajalt.clikt.parameters.options.*
import com.github.ajalt.clikt.parameters.types.file
import com.github.ajalt.clikt.parameters.types.int
import edu.ufrn.lapps.commands.custom.BigCoreOptions
import edu.ufrn.lapps.commands.custom.LittleCoreOptions
import edu.ufrn.lapps.data.Timeout
import edu.ufrn.lapps.system.*
import edu.ufrn.lapps.utils.*
import kotlinx.coroutines.*
import java.util.concurrent.TimeUnit
import kotlin.IllegalArgumentException

enum class Verbose { NONE, INFO, DEBUG }

const val description = """
    Execute a command in a frequency controlled arm big.LITTLE environment.
    
    The frequencies will be set in pairs according to the values passed
    in the -f and -q parameters. In case there is more values specified 
    for one of the clusters, only one of the clusters will have the 
    frequency set.

    EX: -f 10,20,30 -q 10,20,30,40,50
        There will be three runs with the following configurations:
        (big, little) -> (10, 10) Hz
        (big, little) -> (20, 20) Hz
        (big, little) -> (30, 30) Hz
        and then two runs with only the little cluster set to 40 and 50 Hz.
        
    In case only one of the clusters is specified with -f or -q, only that cluster will have
    it's frequencies configured, the other one will be left as is.

    TIP: if options for the COMMAND are being treated as options
    for the execute command, you can use -- to parse everything after it
    as arguments.
"""

class Execute : CliktCommand(help = description) {

    /** Parses the quantity of big cores and the respective frequencies */
    private val big : BigCoreOptions? by BigCoreOptions().cooccurring()

    /** Parses the quantity of LITTLE cores and the respective frequencies */
    private val little : LittleCoreOptions? by LittleCoreOptions().cooccurring()

    /** The number of repetitions of each configuration (if the user specifies 5 configurations of big.LITTLE frequencies
        then each one of them will be run RUNS times). */
    private val runs by option("-n", help = "number of runs").int()
        .default(5)
        .validate {
            if (it <= 0) fail("Number of runs must be greater than 0")
        }

    /** Output file where battery state is going to be saved in csv format */
    private val output by option("-o", "--output", help="File where battery state is going to be saved in csv format.").file()

    /** Outputs some extra info about the run */
    private val verbose by option("-v", "--verbose", help = "prints debugging output").counted()

    /** The command that is meant to be run in a controlled environment (frequency and number of cpus fixed) */
    private val command by argument("COMMAND", help = "command to be run in controlled environment").multiple()
        .validate {
            if (it.isEmpty()) fail("Please specify a command to be run")
        }

    /** Kills the command program after the time defined by [timeout] has passed. (Currently unused) */
    private val timeout by option("-t", "--timeout")
        .validate {
            Timeout.PATTERN.matchEntire(it) ?: fail("Please specify correct units for timeout. One of [ms,s,m,h,d]. " +
                    "--help for more info")
        }

    override fun run() {
        if (big == null && little == null) {
            throw CliktError("Expected either big cluster or LITTLE cluster options to be defined. " +
                    "run kwatts execute --help for more info")
        }

        // Parses the timeout command line argument that allows the user to specify the time unit to be used
        val processTimeout = try {
            timeout?.let { Timeout.parse(it) }
        } catch (e : IllegalArgumentException) {
            // In case the user passes a weird argument to the timeout parameter
            throw CliktError(e.message)
        }

        when (verbose) {
            Verbose.DEBUG.ordinal -> frequencyInfo()
            Verbose.INFO.ordinal -> printCpuConfig()
        }

        little?.let { println("LITTLE frequencies: ${it.frequencies}") }
        big?.let { println("big frequencies: ${it.frequencies}") }

        // Quantities of cores on which the process will be executed for each cluster
        val lcores = little?.littleCores ?: 0
        val bcores = big?.bigCores ?: 0

        val littleIdx = (LITTLE_MIN_IDX until LITTLE_MIN_IDX + lcores)
        val bigIdx = (BIG_MIN_IDX until BIG_MIN_IDX + bcores)

        val cores = littleIdx + bigIdx

        val bigFreq = big?.frequencies?.map { it.toInt() } ?: emptyList()
        val littleFreq = little?.frequencies?.map { it.toInt() } ?: emptyList()

        val logger = BatteryCsvLogger(output)
        logger.writeHeader()

        // mask that defines on which CPUs the process will be run
        val taskmask = cores.toHexMask()
        val scommand = command.joinToString(" ")
        for (freq in bigFreq merge littleFreq) {

            freq.first?.let { value ->
                Processor.setFrequency(Cluster.BIG, value)
            }
            freq.second?.let { value ->
                Processor.setFrequency(Cluster.LITTLE, value)
            }

            // Adds information on the logger about which configuration is running now
            logger.littleFrequency = freq.second ?: -1
            logger.bigFrequency = freq.first ?: -1

            repeat(runs) { iter ->
                echo("This is run ${iter + 1} of $runs")

                logger.iteration = iter

                // Starts monitoring the battery status
                val job = logger.start()
                val (elapsed, start, end) = timeit {
                    "taskset $taskmask $scommand".runProcess(timeout = processTimeout)
                }

                val outputElapsed = TimeUnit.NANOSECONDS.toMillis(elapsed)
                val outputStart = start.toDatetime("dd/MM/yyyy HH:mm:ss")
                val outputEnd = end.toDatetime("dd/MM/yyyy HH:mm:ss")

                println("Run $iter with frequency (big: ${freq.first ?: "None"}, little: ${freq.second ?: "None"}):")
                println("Times -> started: $outputStart, ended: $outputEnd, elapsed: $outputElapsed ms")

                runBlocking { job.cancelAndJoin() }
            }
        }

        logger.destroy()
    }

    private fun frequencyInfo() {
        Processor.setFrequency(Cluster.LITTLE, 1248000)
        Processor.setFrequency(Cluster.BIG, 1248000)

        val output = """
            ====== CPU FREQUENCY INFO ======
            Available frequencies for big cores:
            ${Processor.getFrequencies(Cluster.BIG).joinToString(separator = " ")}
            Available frequencies for LITTLE cores:
            ${Processor.getFrequencies(Cluster.LITTLE).joinToString(separator = " ")}
        """.trimIndent()

        println(output)
    }

    private fun printCpuConfig() {
        val output = """
             CPU Configuration for this run
             ==============================
            BIG_CHOSEN ${ big?.bigCores ?: "None" }
            LITTLE_CHOSEN ${little?.littleCores ?: "None"}
        """.trimIndent()
        println(output)
    }
}