package edu.ufrn.lapps.system

import java.io.File

class KernelFile<T>(val path: String, val transform: (String?) -> T) {

    fun read() : T = transform(readFromFile())

    fun write(value : T) = writeToFile(value)

    private fun readFromFile() : String? {
        return File(path).readLines().firstOrNull()?.trimEnd('\n')
    }

    private fun writeToFile(value: T) {
        val text = value.toString()
        File(path).writeText("$text\n")
    }
}