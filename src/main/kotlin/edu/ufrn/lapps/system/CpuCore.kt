package edu.ufrn.lapps.system

import edu.ufrn.lapps.getCpuPath
import java.io.File

/**
 * Because of the volatility of sysfs, some files might not be available when the CPU is turned off.
 * I need to think a way to make these classes represent state rather then static files.
 * The way it's implemented now, if any core is turned off, the code still treats it as if its
 * online, which will cause file not found exceptions.
*/
class CpuCore(val index : Int) {
    private val path : String
    val cluster : Cluster

    init {
        val cpuPath = getCpuPath(virtual = true)
        path = "$cpuPath/cpu$index"
        cluster = getClusterType(path)
    }

    private fun getClusterType(cpuPath: String) : Cluster {
        val topologyPath = "$cpuPath/topology/physical_package_id"
        val clusterType = File(topologyPath)
            .readLines()
            .first()
            .trimEnd('\n')
            .toInt()

        return if (clusterType == 1) {
            Cluster.LITTLE
        } else {
            Cluster.BIG
        }
    }

    val maxFrequency = KernelFile("$path/cpufreq/scaling_max_freq") { content -> content?.toInt() ?: -1 }

    val minFrequency = KernelFile("$path/cpufreq/scaling_min_freq") { content -> content?.toInt() ?: -1 }

    val governor = KernelFile("$path/cpufreq/scaling_governor") { governor -> governor ?: "Unknown" }

    val currentFrequency = KernelFile("$path/cpufreq/scaling_cur_freq") { it?.toInt() ?: -1 }

    // Frequencies in Hertz
    val availableFrequencies = KernelFile("$path/cpufreq/scaling_available_frequencies") { it?.split(" ")?.map { str -> str.toInt() } ?: emptyList() }

    val availableGovernors = KernelFile("$path/cpufreq/scaling_available_governors") { it?.split(" ") ?: emptyList() }

}
