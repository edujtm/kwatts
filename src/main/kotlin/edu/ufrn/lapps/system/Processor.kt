package edu.ufrn.lapps.system

import java.io.File

enum class Cluster { BIG, LITTLE }

const val LITTLE_MIN_IDX = 0
const val LITTLE_MAX_IDX = 5
const val BIG_MIN_IDX = 6
const val BIG_MAX_IDX = 7
const val THERMAL_ZONES = 5

object Processor {
    val numCores: Int
    val cores : List<CpuCore>
    val virtualThermalZone : List<ThermalZone>
    val hardwareThermalZone : List<ThermalZone>

    init {
        numCores = getQntCores()
        cores = getCores(numCores)
        virtualThermalZone = getThermalZones(virtual = true)
        hardwareThermalZone = getThermalZones(virtual = false)
    }

    private fun getQntCores() : Int {
        return File("/proc/cpuinfo")
            .readLines()
            .filter { it.startsWith("processor") }
            .size
    }

    private fun getCores(qnt: Int) : List<CpuCore> = (0 until qnt).map { CpuCore(it) }

    private fun getThermalZones(virtual: Boolean) = (0 until THERMAL_ZONES).map { ThermalZone(it, virtual) }

    fun getClusterSize(cluster: Cluster) = cores.filter { it.cluster == cluster }.size

    fun getClusterMinIdx(cluster: Cluster) = cores.sortedBy { it.index }.first { it.cluster == cluster }.index

    fun getClusterMaxIdx(cluster: Cluster) = cores.sortedBy { it.index }.last { it.cluster == cluster}.index

    // TODO move thermal states to a more appropiate place
    fun getState(virtual: Boolean = true) : ProcessorState {
        val frequencies = ArrayList<Int>()
        val thermal = ArrayList<Int>()

        val thermalList = if (virtual) virtualThermalZone else hardwareThermalZone

        val governor = cores.first().governor.read()
        for (core in cores) {
            frequencies.add(core.currentFrequency.read())
        }
        for (tz in thermalList) {
            thermal.add(tz.temperature.read() / 100)
        }

        return ProcessorState(virtual, governor, frequencies, thermal)
    }

    fun setFrequency(cluster: Cluster, frequency: Int) {
        val idx = when (cluster) {
            Cluster.BIG -> BIG_MIN_IDX
            Cluster.LITTLE -> LITTLE_MIN_IDX
        }

        cores[idx].run {
            if (frequency > maxFrequency.read()) {
                maxFrequency.write(frequency)
                minFrequency.write(frequency)
            } else {
                minFrequency.write(frequency)
                maxFrequency.write(frequency)
            }
        }
    }

    fun getFrequencies(cluster: Cluster) : List<Int> {
        val idx = when (cluster) {
            Cluster.BIG -> BIG_MIN_IDX
            Cluster.LITTLE -> LITTLE_MIN_IDX
        }

        return cores[idx].availableFrequencies.read()
    }

}