package edu.ufrn.lapps.system

import edu.ufrn.lapps.getThermalPath

class ThermalZone(val index: Int, virtual : Boolean = true) {
    private val path : String
    val tripPoints : List<KernelFile<Int>>

    init {
        val thermalPath = getThermalPath(virtual)
        path = "$thermalPath/thermal_zone$index"
        tripPoints = (0..7).map { idx ->
            KernelFile("$path/trip_point_${idx}_temp") { it?.toInt() ?: -1 }
        }
    }

    val temperature = KernelFile("$path/temp") { it?.toInt() ?: -1 }

    // Set trip points is not working due to trip_point files on Samsung A8 being read-only
    fun setTripPoints(temperatures: List<Int>) {
        if (temperatures.size != tripPoints.size) {
            throw IllegalArgumentException("Could not set temperatures trip points for thermal zone $index.")
        }

        for ((tripPoint, temperature) in tripPoints.zip(temperatures)) {
            tripPoint.write(temperature)
        }
    }
}