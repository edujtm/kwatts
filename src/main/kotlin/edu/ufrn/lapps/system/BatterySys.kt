package edu.ufrn.lapps.system

enum class BatteryHealth {
    COLD, DEAD, GOOD, OVERHEAT, OVERVOLTAGE, UNKNOWN
}

enum class BatteryStatus {
    CHARGING, DISCHARGING, FULL, NOTCHARGING, UNKNOWN
}

object BatterySys {
    const val path = "/sys/class/power_supply/battery"

    // units in tenths of centigrate
    val temperature = KernelFile("$path/batt_temp") { content -> content?.toInt() ?: -1 }

    // files for currentNow and currentAvg might not be available on all devices
    // units in uA (microAmpere) as stated in kernel documentation
    val currentNow = KernelFile("$path/current_now") { current -> current?.toInt() ?: -1 }

    // units in uA (microAmpere) as stated in kernel documentation
    val currentAvg = KernelFile("$path/current_avg") { current -> current?.toInt() ?: -1 }

    // units in uV (microVolts)
    val voltageNow = KernelFile("$path/voltage_now") { voltage -> voltage?.toInt() ?: -1 }

    // units in uV (microVolts)
    val voltageAvg = KernelFile("$path/voltage_avg") { voltage -> voltage?.toInt() ?: -1 }

    // battery percentage
    val capacity = KernelFile("$path/capacity") { capacity -> capacity?.toInt() ?: -1 }

    // One of [Cold, Dead, Good, Overheat, Over Voltage, Unknown]
    val health = KernelFile("$path/health") { health ->
        val id = health?.filter { it != ' ' }?.toUpperCase() ?: "UNKNOWN"

        try {
            BatteryHealth.valueOf(id)
        } catch (e : IllegalArgumentException) {
            BatteryHealth.UNKNOWN
        }
    }

    // One of [Charging, Discharging, Full, Not Charging, Unknown]
    val status = KernelFile("$path/status") { status ->
        val id = status?.filter { it != ' ' }?.toUpperCase() ?: "UNKNOWN"

        try {
            BatteryStatus.valueOf(id)
        } catch (e: IllegalArgumentException) {
            BatteryStatus.UNKNOWN
        }
    }

    fun getCsvHeader() : String {
        return "current_now,current_avg,voltage_now,voltage_avg,batt_temp,capacity,health,status"
    }

    fun format(separator : String = "\t", width : Int = 0) : String {
        return listOf<KernelFile<*>>(currentNow, currentAvg, voltageNow,
            voltageAvg, temperature, capacity, health, status)
            .map { it.read() }
            .joinToString(separator) {
                it.toString().padEnd(width)
            }
    }
}