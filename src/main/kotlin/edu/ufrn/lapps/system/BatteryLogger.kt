package edu.ufrn.lapps.system

import kotlinx.coroutines.*
import java.io.File
import java.io.PrintWriter
import kotlin.coroutines.CoroutineContext

/**
 * This logger needs to join together information from the different configurations and the battery
 * state (e.g. current, voltage, health) when the process is executing and save this information into
 * a csv file in a way that makes it easier to parse it later.
 */
class BatteryCsvLogger(outputFile: File?, val samplingRate: Long = 500L) : CoroutineScope {

    private val job = Job()
    private val output = getOutputStream(outputFile)
    var bigFrequency = -1
    var littleFrequency = -1
    var iteration = -1

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job

    private fun getOutputStream(file: File?) : PrintWriter {
        val outputStream = file?.outputStream() ?: System.out
        return PrintWriter(outputStream, true)
    }

    fun writeHeader() {
        val cpuTempHeader = (0..4).joinToString(separator = ",") { idx -> "thermalzone_temp$idx" }
        val header = "timestamp,iteration,little,big,$cpuTempHeader,${BatterySys.getCsvHeader()}"
        output.println(header)
    }

    fun start() = launch {
        while (isActive) {
            val timestamp = System.currentTimeMillis()
            // This is horrible but whatever
            val temperatures = Processor.getState(virtual = true).temperatures.joinToString(separator = ",")
            val dataline = "$timestamp,$iteration,$littleFrequency,$bigFrequency,$temperatures,${BatterySys.format(separator = ",")}"
            output.println(dataline)
            delay(samplingRate)
        }
    }

    fun destroy() = runBlocking {
        job.cancelAndJoin()
        output.close()
    }
}