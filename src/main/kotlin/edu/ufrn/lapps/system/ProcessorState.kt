package edu.ufrn.lapps.system

import java.lang.StringBuilder

fun processorHeader(separator : String = "", width : Int = 0) : String {
    val sb = StringBuilder("VIRTUAL_SCALING$separator".padEnd(width))
    for (i in 0 until 8) {
        sb.append("CPU${i}_FREQ$separator".padEnd(width))
    }
    sb.append("GOVERNOR".padEnd(width))
    for (i in 0 until 4) {
        sb.append("${separator}CPU${i}_TEMP".padEnd(width))
    }
    return sb.toString()
}

data class ProcessorState(val virtualScaling : Boolean, val governor : String, val frequencies: List<Int>, val temperatures: List<Int>) {

    fun format(separator: String = "\t", width: Int = 0) : String {
        val initial = if (virtualScaling) "1" else "0"
        val freqString = frequencies.joinToString(separator)
        val thermalString = temperatures.joinToString(separator)
        return listOf<Any>(initial, freqString, governor, thermalString).joinToString(separator) {
            it.toString().padEnd(width)
        }
    }

}
