package edu.ufrn.lapps.data

import java.util.concurrent.TimeUnit

/*
 * A simple mapper from the available unit options to TimeUnit. It's used
 * to parse the command line with the available options
 */
enum class UnitOptions(val stringValue : String, val unit: TimeUnit) {
    MILLISECONDS("ms", TimeUnit.MILLISECONDS),
    SECONDS("s", TimeUnit.SECONDS),
    MINUTES("m", TimeUnit.MINUTES),
    HOURS("h", TimeUnit.HOURS),
    DAYS("d", TimeUnit.DAYS);

    companion object {
        fun matchBy(matcher: (UnitOptions) -> Boolean) : UnitOptions? {
            return values().find(matcher)
        }
    }
}
