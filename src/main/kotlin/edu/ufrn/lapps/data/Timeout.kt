package edu.ufrn.lapps.data

import java.util.concurrent.TimeUnit

data class Timeout(val time: Long, val unit: TimeUnit) {

    companion object {
        fun parse(timeout: String) : Timeout {
            val match = PATTERN.find(timeout)
            return try {
                val time = match!!.groups[1]!!.value.toLong()
                if (time <= 0) throw IllegalArgumentException("Timeout cannot be <= 0: $timeout")

                val unitString = match.groups[2]?.value ?: "s"
                val unit = UnitOptions.matchBy { it.stringValue == unitString }?.unit ?: TimeUnit.SECONDS
                Timeout(time, unit)
            } catch (e: java.lang.NullPointerException) {
                throw IllegalArgumentException("Error while parsing timeout argument string: $timeout")
            }
        }

        /**
            The pattern to which the command line argument needs to conform.
            In case the unit is not specified, It's assumed to be in seconds.
            Ex: 10h, 100s, 100, 2500ms
         */
        val PATTERN :Regex
            get() {
                val options = UnitOptions.values().joinToString(separator = "|") { it.stringValue }
                return Regex("(\\d+)($options)?")
            }
    }
}