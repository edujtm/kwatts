@file:JvmName("Main")

package edu.ufrn.lapps

import com.github.ajalt.clikt.core.subcommands
import edu.ufrn.lapps.commands.Battery
import edu.ufrn.lapps.commands.Execute
import edu.ufrn.lapps.commands.Kwatts
import edu.ufrn.lapps.commands.Temperature

fun main(args: Array<String>) = Kwatts()
    .subcommands(Battery(), Execute(), Temperature())
    .main(args)

