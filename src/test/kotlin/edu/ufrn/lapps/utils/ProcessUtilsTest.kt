package edu.ufrn.lapps.utils

import edu.ufrn.lapps.data.Timeout
import io.kotlintest.matchers.string.shouldContain
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.kotlintest.specs.FunSpec
import org.apache.commons.lang3.SystemUtils
import java.util.concurrent.TimeUnit

class ProcessUtilsTest : FunSpec({
    context("launching processes with runProcess") {
        test("should throw exception when return is non zero")
            .config(enabled = SystemUtils.IS_OS_LINUX) {
            val command = "cat this-file-should-not_exist.nonvalid"

            val exception = shouldThrow<RuntimeException> {
                command.runProcess()
            }

            exception.message shouldContain command
        }

        // This test might fail on some linux distributions
        test("should throw exception when timeout is reached")
            .config(enabled = SystemUtils.IS_OS_LINUX) {
            val command = "sleep 0.1"
            val time = Timeout(50, TimeUnit.MILLISECONDS)

            val exception = shouldThrow<RuntimeException> {
                command.runProcess(timeout = time)
            }

            exception.message shouldContain "timed out"
            exception.message shouldContain command
        }
    }

    context("transforming iterable of integers into a hex mask") {
        test("should transform list of integer into a string hex mask") {
            val cores = listOf(0, 2, 4, 6)

            val mask = cores.toHexMask()

            mask shouldContain "55"
        }
    }
})