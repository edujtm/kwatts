package edu.ufrn.lapps.utils

import io.kotlintest.inspectors.forAll
import io.kotlintest.inspectors.forExactly
import io.kotlintest.matchers.collections.shouldBeEmpty
import io.kotlintest.matchers.collections.shouldContain
import io.kotlintest.matchers.collections.shouldHaveSize
import io.kotlintest.shouldBe
import io.kotlintest.shouldHave
import io.kotlintest.shouldNotBe
import io.kotlintest.specs.FunSpec
import kotlinx.coroutines.delay
import java.util.concurrent.TimeUnit

class UtilsTest : FunSpec({
    context("merging iterables") {
        test("iterables with different lengths should contain nulls") {
            val fruits = listOf("apple", "orange", "banana")
            val coins = listOf(1, 5, 10, 25, 50)

            val merged = (fruits merge coins).asSequence().toList()

            merged shouldHaveSize coins.size
            merged shouldContain Pair(null, 25)
            merged shouldContain Pair(null, 50)
        }

        test("iterables with same length should not contain nulls") {
            val pokemons = listOf("chamander", "bulbasaur", "squirtle", "pikachu")
            val sodas = listOf("coca-cola", "fanta", "kuat", "pepsi")

            val merged = (pokemons merge sodas).asSequence().toList()

            merged shouldHaveSize pokemons.size
            merged.forAll {
                it.first shouldNotBe null
                it.second shouldNotBe null
            }
        }

        test("should pair with null when one list is empty") {
            val pokemons = listOf("chamander", "bulbasaur", "squirtle", "pikachu")
            val none = emptyList<Any>()

            val merged = (pokemons merge none).asSequence().toList()

            merged shouldHaveSize pokemons.size
            merged.forAll { it.second shouldBe null }
        }

        test("should return empty iterator when both are empty") {
            val first = emptyList<Any>()
            val second = emptyList<Any>()

            val merged = (first merge second).asSequence().toList()

            merged.shouldBeEmpty()
        }
    }
})
