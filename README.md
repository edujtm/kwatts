# Kwatts

A platform for execution and monitoring of parallel programs in a controlled environment,
with Energy Aware Scheduling (EAS) in mind on the Android platform.

- Warning: Your phone needs to be rooted in order to execute this application. Changing CPU frequency configuration recklessly can damage your device.

## How to install

*TODO: Make this how to install simpler. The process is way more convoluted than I thought, it doesn't seem that complex with the environment already setup*

- *First you need to get the Android SDK from the *[Android Developers Website](https://developer.android.com/studio/index.html)*. Sadly this might be a little convoluted process if you're not familiar with it.*

This has to be done because this project uses the D8 compiler to generate Dex binaries so they can run on the Dalvik VM. The D8 compiler comes with the SDK tools. 

The version of the build tools used in this project was 29.0.1 but I believe it might work with future versions.*

**The easiest way to do that is to download [Android Studio](https://developer.android.com/studio/) and the use the [SDK Manager](https://developer.android.com/studio/intro/update#sdk-manager) to download the right version of the SDK.**

After downloading Android Studio you need to go into `Tools > SDK Manager` and look for Android SDK Build-Tools v29.0.1.

This might be easier if you prefer using IDEs, but Android Studio needs a lot of disk space and it is completely unecessary for this application.

**The other option is to Download the `sdkmanager` command line tool only and use it to get the right version of the sdk, but the sdkmanager tool is outdated and will not work properly if you have a newer version of Java (OpenJDK 11+).**

If you have an older version of java, you can run the following (on linux):

```
wget https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip 
unzip sdk-tools-linux-4333796.zip -d sdk-tools/
export ANDROID_SDK=$HOME/Android/Sdk/
cd sdk-tools/tools/bin/
./sdkmanager --install "build-tools;29.0.1 --sdk-root=$ANDROID_SDK"
```

if you have a newer version of java you can try downloading the newer (unstable) version of the command line tools:

```
wget https://dl.google.com/android/repository/commandlinetools-linux-5842447_latest.zip
unzip commandlinetools-linux-5842447_latest.zip -d sdk-tools/
export ANDROID_SDK=$HOME/Android/Sdk/
cd sdk-tools/tools/bin/	
./sdkmanager --install "build-tools;29.0.1 --sdk-root=$ANDROID_SDK"
```

- After you managed to install the build-tools v29.0.1, run `export D8C=$ANDROID_SDK/build-tools/29.0.1/d8c`.

- now you can run `./gradlew zipDex`, this will create a zip file in the build directory 

- Finally connect your phone and run (some of these steps requires your device to be rooted):

```
adb push build/Kwatts.zip /sdcard/
adb push kwatts /sdcard/
adb shell
mount -o rw,remount /system
mv /sdcard/kwatts /system/bin/
mount -o ro,remount /system
```

- Now you can run (from the adb shell session) `kwatts --help`.

## Acknowledgements

This project is completely based on the work by [Demetrios Coutinho](https://gitlab.com/demetriosamc) 
in [XU3EM](https://gitlab.com/lappsufrn/XU3EM) which is in turn a fork of [DATACOLLECT](https://github.com/kranik/ARMPM_DATACOLLECT) 
by [Kris Nikov](https://github.com/kranik)
